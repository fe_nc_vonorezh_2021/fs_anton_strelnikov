const palindrome = () => {
    let word = prompt("Введите слово").replace(/\s+/g, "");
    // Преобразуем в нижний регистр, чтобы избежать проблем с заглавными буквами
    let new_word = word.toLowerCase();
    if (new_word == new_word.split("").reverse().join("")) {
        alert(word + " палиндром!");
    } else {
        alert(word + " не палиндром!");
    }
};
palindrome();
