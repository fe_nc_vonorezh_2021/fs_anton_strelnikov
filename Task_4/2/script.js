const getName = () => {
    let person_name = prompt("Ваше имя:");
    return (person_name = person_name.charAt(0).toUpperCase() + person_name.slice(1));
};

const getAge = () => {
    let age = +prompt("Ваш возраст:");
    if (age == "" || age < 0 || isNaN(age)) {
        alert("Введите корректный возраст!");
        return getAge();
    } else {
        return age;
    }
};

const alertNameAndAge = (name, age) => {
    let count = age % 100;
    const alertStr = (typeOfAge) => alert(`Привет, ${name}, тебе уже ${age} ${typeOfAge}!`);
    if (count >= 5 && count <= 20) {
        alertStr("лет");
    } else {
        count = count % 10;
        if (count === 1) {
            alertStr("год");
        } else if (count >= 2 && count <= 4) {
            alertStr("года");
        } else {
            alertStr("лет");
        }
    }
};

alertNameAndAge(getName(), getAge());
