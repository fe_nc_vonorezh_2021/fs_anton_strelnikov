//С использование третьей переменной
let a = 5;
let b = 2;
console.log("Начальные переменные: A = " + a + " B = " + b);
const old = document.getElementById("old");
old.innerHTML = "A = " + a + " " + "B = " + b;
let c = a;
a = b;
b = c;
console.log("Измененные переменные: A = " + a + " B = " + b);
const changed = document.getElementById("changed");
changed.innerHTML = "A: " + a + " " + "B: " + b;
// Только переменные a и b
console.log("Начальные переменные: A = " + a + " B = " + b);
const old2 = document.getElementById("old2");
old2.innerHTML = "A: " + a + " " + "B: " + b;
// Используем деструктуризацию
[a, b] = [b, a];
console.log("Измененные переменные: A = " + a + " B = " + b);
const changed2 = document.getElementById("changed2");
changed2.innerHTML = "A: " + a + " " + "B: " + b;
