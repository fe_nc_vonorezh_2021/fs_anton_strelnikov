function game() {
    let random_number = Math.floor(Math.random() * 1000) + 1;
    let count = 0;
    const header = document.getElementById("header");
    const getNumber = () => {
        let number = prompt("Введите число:");
        if (number == "" || isNaN(number)) {
            alert("Введите число!");
            getNumber();
        } else if (random_number < number) {
            alert("Искомое число меньше");
            count++;
            getNumber();
        } else if (random_number > number) {
            alert("Искомое число больше");
            count++;
            getNumber();
        } else if (random_number == number) {
            let end = confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`);
            if (end) {
                random_number = Math.floor(Math.random() * 1000) + 1;
                getNumber();
            } else if (!end) header.innerHTML = "Спасибо за игру";
        }
    };
    getNumber();
}
