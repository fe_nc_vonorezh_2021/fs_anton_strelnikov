import { Component, OnInit, Input } from "@angular/core";
import { Animal } from "../animal";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { AnimalService } from "../animal.service";
import { FormGroup, FormControl, FormsModule } from "@angular/forms";
import { Validators } from "@angular/forms";
import { AnimalModuleModule } from "../animal-module.module";

@Component({
    selector: "app-animal-details",
    templateUrl: "./animal-details.component.html",
    styleUrls: ["./animal-details.component.less"],
})
export class AnimalDetailsComponent implements OnInit {
    animal: Animal | undefined;
    animalForm: FormGroup | undefined;

    constructor(
        private route: ActivatedRoute,
        private animalService: AnimalService,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.getAnimal();
        setTimeout(() => this.setValues(), 200);
    }

    getAnimal(): void {
        const id = parseInt(this.route.snapshot.paramMap.get("id")!, 10);
        this.animalService
            .getAnimal(id)
            .subscribe((animal) => (this.animal = animal));
    }

    goBack(): void {
        this.location.back();
    }

    updateAnimal(): void {
        this.animal! = this.animalForm!.value;
    }
    save(): void {
        if (this.animal) {
            this.animalService.updateAnimal(this.animal, this.animal.id);
        }
    }

    onSubmit() {
        this.updateAnimal();
        this.save();
    }

    setValues() {
        this.animalForm = new FormGroup({
            name: new FormControl(this.animal!.name, [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(10),
            ]),
            id: new FormControl(this.animal?.id),
            type: new FormControl(this.animal?.type, [Validators.required]),
            color: new FormControl(this.animal?.color),
            age: new FormControl(this.animal?.age, [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(2),
            ]),
            gender: new FormControl(this.animal?.gender),
        });
    }

    get name() {
        return this.animalForm!.get("name")!;
    }

    get color() {
        return this.animalForm!.get("color")!;
    }
    get age() {
        return this.animalForm!.get("age")!;
    }
}
