import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Animal } from "./animal";
import { Animals } from "./animal-list";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class AnimalService {
    private animalsUrl = "http://localhost:3000/animals";

    httpOptions = {
        headers: new HttpHeaders({ "Content-Type": "application/json" }),
    };

    constructor(private http: HttpClient) {}

    getAnimals(): Observable<Animal[]> {
        return this.http
            .get<Animal[]>(this.animalsUrl)
            .pipe(catchError(this.handleError<Animal[]>("getAnimals", [])));
    }

    getFilteredAnimals(): Observable<Animal[]> {
        const animals = of(Animals);
        const filteredAnimals = animals.pipe(
            map((animal) =>
                animal.filter(
                    (animal) => animal.type != "Кот" && animal.type != "Кошка"
                )
            )
        );
        return filteredAnimals;
    }

    getAnimal(id: number): Observable<Animal> {
        const url = `${this.animalsUrl}/${id}`;

        return this.http
            .get<Animal>(url)
            .pipe(catchError(this.handleError<Animal>(`getAnimal id=${id}`)));
    }

    updateAnimal(animal: Animal, id: number): Observable<any> {
        const url = `${this.animalsUrl}/${id}`;
        return this.http
            .put(url, animal, this.httpOptions)
            .pipe(catchError(this.handleError<any>("updateAnimal")));
    }
    addAnimal(animal: Animal): Observable<Animal> {
        return this.http
            .post<Animal>(this.animalsUrl, animal, this.httpOptions)
            .pipe(catchError(this.handleError<Animal>("addAnimal")));
    }

    deleteAnimal(id: number): Observable<Animal> {
        const url = `${this.animalsUrl}/${id}`;
        return this.http
            .delete<Animal>(url, this.httpOptions)
            .pipe(catchError(this.handleError<Animal>("deleteHero")));
    }
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            return of(result as T);
        };
    }
}
