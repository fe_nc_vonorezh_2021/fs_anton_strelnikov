import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AnimalComponent } from "./animal/animal.component";
import { AnimalDetailsComponent } from "./animal-details/animal-details.component";

import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { AddAnimalComponent } from "./add-animal/add-animal.component";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";
@NgModule({
    declarations: [AnimalComponent, AnimalDetailsComponent, AddAnimalComponent],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule,
    ],
    exports: [AnimalComponent, AnimalDetailsComponent],
    providers: [HttpClientModule],
})
export class AnimalModuleModule {}
