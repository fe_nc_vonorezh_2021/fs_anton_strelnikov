import { Component, OnInit } from "@angular/core";
import { Animal } from "../animal";
import { AnimalService } from "../animal.service";

@Component({
    selector: "app-animal",
    templateUrl: "./animal.component.html",
    styleUrls: ["./animal.component.less"],
})
export class AnimalComponent implements OnInit {
    hiddenCats: boolean = false;
    animals: Animal[] = [];

    constructor(public animalService: AnimalService) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    getAnimals(): void {
        this.hiddenCats = false;
        this.animalService
            .getAnimals()
            .subscribe((animals) => (this.animals = animals));
    }

    delete(animal: Animal): void {
        this.animals = this.animals.filter((h) => h !== animal);
        this.animalService.deleteAnimal(animal.id).subscribe();
    }
}
