import { Component, Input } from "@angular/core";
import { Animal } from "../animal";
import { Location } from "@angular/common";
import { AnimalService } from "../animal.service";
import { ViewChild } from "@angular/core";

@Component({
    selector: "app-add-animal",
    templateUrl: "./add-animal.component.html",
    styleUrls: ["./add-animal.component.less"],
})
export class AddAnimalComponent {
    @ViewChild("animalForm") form: any;
    animals: Animal[] = [];
    genders = ["Мальчик", "Девочка"];
    animal = {
        id: "",
        name: "",
        type: "",
        color: "",
        age: "",
        gender: this.genders[0],
    };

    constructor(
        private animalService: AnimalService,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    onSubmit(): void {
        this.getValues();
        this.save();
    }

    getValues(): void {
        if (this.form.valid) {
            console.log(this.form.value);
        }
    }

    save(): void {
        if (this.form.value) {
            this.animalService
                .addAnimal(this.form.value as Animal)
                .subscribe((animal) => {
                    this.animals.push(animal);
                    this.goBack();
                });
        }
    }

    getAnimals(): void {
        this.animalService
            .getAnimals()
            .subscribe((animals) => (this.animals = animals));
    }

    goBack(): void {
        this.location.back();
    }
}
