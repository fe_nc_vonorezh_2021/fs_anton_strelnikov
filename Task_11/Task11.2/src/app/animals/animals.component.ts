import { Component, OnDestroy, OnInit } from "@angular/core";
import { Animal } from "../animal";
import { AnimalService } from "../animal.service";
import { Subscription } from "rxjs";
@Component({
    selector: "app-animals",
    templateUrl: "./animals.component.html",
    styleUrls: ["./animals.component.css"],
})
export class AnimalsComponent implements OnInit, OnDestroy {
    public selectedAnimal?: Animal;
    public hiddenCats: boolean = false;
    public animals: Animal[] = [];
    private subscription: Subscription[] = [];

    constructor(public animalService: AnimalService) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    onSelect(animal: Animal): void {
        this.selectedAnimal = animal;
    }

    getAnimals(): void {
        this.hiddenCats = false;
        this.subscription.push(this.animalService.getAnimals().subscribe((animals) => (this.animals = animals)));
    }

    getFilteredAnimals(): void {
        this.hiddenCats = true;
        this.subscription.push(this.animalService.getFilteredAnimals().subscribe((animals) => (this.animals = animals)));
    }

    ngOnDestroy(): void {
        this.subscription.forEach((subscription) => subscription.unsubscribe());
    }
}
