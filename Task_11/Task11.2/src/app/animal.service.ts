import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { Animal } from "./animal";
import { animals } from "./animal-list";

@Injectable({
    providedIn: "root",
})
export class AnimalService {
    getAnimals(): Observable<Animal[]> {
        return of(animals);
    }

    getFilteredAnimals(): Observable<Animal[]> {
        const filteredAnimals = of(animals).pipe(map((animal) => animal.filter((animal) => animal.type != "Кот" && animal.type != "Кошка")));
        return filteredAnimals;
    }
}
