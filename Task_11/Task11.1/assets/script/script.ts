const displayCity: HTMLElement = <HTMLElement>document.getElementById("displayCity");
const temp: HTMLElement = <HTMLElement>document.getElementById("temp");
const tempFeels: HTMLElement = <HTMLElement>document.getElementById("temp-feels");
const condition: HTMLElement = <HTMLElement>document.getElementById("condition");
const time: HTMLElement = <HTMLElement>document.getElementById("time");
const icon: HTMLElement = <HTMLElement>document.getElementById("icon");
const humidity: HTMLElement = <HTMLElement>document.getElementById("humidity");
const wind: HTMLElement = <HTMLElement>document.getElementById("wind");
const searchBtn: HTMLElement = <HTMLElement>document.getElementById("search-btn");

const getCurrentWeather = async (): Promise<any> => {
    const cityName: HTMLInputElement = <HTMLInputElement>document.getElementById("cityName");
    const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName.value}&lang=ru&appid=87a4e3b57fd5877a435275e9fae66ccd`);
    const data = await response.json();
    response.ok ? (insertData(data), hourlyWeather(cityName.value)) : errorMessage();
};

const errorMessage = (): void => {
    displayCity.innerHTML = "Город не найден";
    displayCity.style.color = "#d1423d";
};

const insertData = (data: any): void => {
    displayCity.style.color = "#eceef0";
    const date = new Date();
    time.innerHTML = String(date.getHours()) + `:` + String((date.getMinutes() < 10 ? "0" : "") + date.getMinutes());
    displayCity.innerHTML = data.name;
    temp.innerHTML = Math.round(data.main.temp - 273) + ` ℃`;
    tempFeels.innerHTML = `Ощущается как ` + Math.round(data.main.feels_like - 273) + ` ℃`;
    condition.innerHTML = data.weather[0].description[0].toUpperCase() + data.weather[0].description.slice(1);
    icon.innerHTML = `<img src="http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png"></img>`;
    humidity.innerHTML = `Влажность: ` + data.main.humidity + `%`;
    wind.innerHTML = `Скорость ветра: ` + data.wind.speed + ` м/с`;
};

const hourlyWeather = async (cityName: string): Promise<any> => {
    const weatherCardsTemp: HTMLCollection = <HTMLCollection>document.getElementsByClassName("weather-card-temp");
    const weatherCardsIcon: HTMLCollection = <HTMLCollection>document.getElementsByClassName("weather-card-icon");
    const weatherCardsTime: HTMLCollection = <HTMLCollection>document.getElementsByClassName("weather-card-time");
    const response = await fetch(`http://api.openweathermap.org/data/2.5/find?q=${cityName}&lang=ru&cnt=5&appid=87a4e3b57fd5877a435275e9fae66ccd`);
    const data = await response.json();
    const date = new Date();
    for (let i: number = 0; i < data.list.length; i++) {
        Array.from(weatherCardsTemp)[i].innerHTML = Math.round(data.list[i].main.temp - 273) + ` ℃`;
        Array.from(weatherCardsIcon)[i].innerHTML = `<img src="http://openweathermap.org/img/wn/${data.list[i].weather[0].icon}@2x.png"></img>`;
        Array.from(weatherCardsTime)[i].innerHTML = date.getHours() + 1 + i + `:00`;
    }
};

searchBtn.addEventListener("click", getCurrentWeather);
