const quicksort = (array: number[]): number[] => {
    if (array.length <= 1) {
        return array;
    }
    let pivot: number = array[0];
    let left: number[] = [];
    let right: number[] = [];
    for (let i: number = 1; i < array.length; i++) {
        array[i] < pivot ? left.push(array[i]) : right.push(array[i]);
    }

    return quicksort(left).concat(pivot, quicksort(right));
};

const unsorted: number[] = [];
for (let i: number = 0; i < 10; i++) {
    unsorted[i] = Math.floor(Math.random() * 100);
}
console.log(`Unsorted array: ${unsorted}`);
const sorted = quicksort(unsorted);
console.log(`Sorted array: ${sorted}`);
