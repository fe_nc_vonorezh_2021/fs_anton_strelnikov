export class ListNode<T> {
    public next: ListNode<T> | null;
    public prev: ListNode<T> | null;
    public value: T;

    constructor(value: T) {
        this.value = value;
        this.next = null;
        this.prev = null;
    }
}
