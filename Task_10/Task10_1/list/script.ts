import { LinkedList } from "./doubleList";

const list: LinkedList<number> = new LinkedList();

list.insertInBegin(1);
list.insertAtEnd(2);
list.insertAtEnd(3);
list.insertAtEnd(4);
list.insertAtEnd(5);
console.log(list.print());
console.log(list.getElement(2));
list.insertByIndex(10, 4);
console.log(list.print());
list.remove(10);
list.edit(1, 5);
console.log(list.print());
