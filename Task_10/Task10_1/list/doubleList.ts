import { ListNode } from "./listNode";

export class LinkedList<T> {
    private head: ListNode<T> | null;
    private tail: ListNode<T> | null;
    private size: number;

    constructor() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public isEmpty(): boolean {
        return this.size == 0;
    }

    public getElement(index: number) {
        if (index > this.size || this.isEmpty()) {
            console.log("Incorrect index");
            return null;
        } else {
            let current: ListNode<T> | any = this.head;
            for (let i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
    }

    public print() {
        const array: T[] = [];
        if (!this.head) {
            return array;
        }
        const addToArray = (node: ListNode<T>): T[] => {
            array.push(node.value);
            return node.next ? addToArray(node.next) : array;
        };
        return addToArray(this.head);
    }

    public insertInBegin(value: T) {
        const node = new ListNode(value);
        if (!this.head) {
            this.head = node;
        } else {
            this.head.prev = node;
            node.next = this.head;
            this.head = node;
        }
        this.size++;
    }

    public insertAtEnd(value: T) {
        const node = new ListNode(value);
        if (!this.head) {
            this.head = node;
        } else {
            const getLast = (node: ListNode<T>): ListNode<T> => {
                return node.next ? getLast(node.next) : node;
            };
            const lastNode = getLast(this.head);
            node.prev = lastNode;
            lastNode.next = node;
        }
        this.size++;
    }

    public insertByIndex(value: T, index: number) {
        if (index > this.size || this.isEmpty()) {
            console.log("Incorrect index");
        }
        const node = new ListNode(value);
        if (index === 0) {
            if (this.size === 0) {
                this.tail = node;
            } else {
                this.head!.prev = node;
            }
            node.next = this.head;
            this.head = node;
        } else {
            let current: ListNode<T> | any = this.head;
            for (let i = 0; i < index - 1; i++) {
                current = current.next;
            }
            node.next = current;
            node.prev = current.prev;
            current.prev.next = node;
            current.prev = node;
        }
        this.size++;
    }

    public remove(value: any) {
        if (this.isEmpty()) {
            console.log("List is empty");
        }
        let current = this.head;
        while (current != null) {
            if (current.value === value) {
                if (current.prev != null) {
                    current.prev.next = current.next;
                } else {
                    this.head = current.next;
                }
                if (current.next != null) {
                    current.next.prev = current.prev;
                } else {
                    this.tail = current.prev;
                }
                this.size--;
                return;
            }
            current = current.next;
        }
    }

    public edit(index: number, value: T) {
        if (index > this.size || this.isEmpty()) {
            console.log("Incorrect index");
        } else {
            let current: ListNode<T> | any = this.head;
            for (let i = 0; i < index; i++) {
                current = current.next;
            }
            current.value = value;
        }
    }
}
