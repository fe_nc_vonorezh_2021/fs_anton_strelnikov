import { fromEvent, merge } from "rxjs";

const body: any = document.querySelector("body");
const getRandBackColor = (): string => {
    const red: number = Math.floor(Math.random() * 255);
    const green: number = Math.floor(Math.random() * 255);
    const blue: number = Math.floor(Math.random() * 255);
    const str: string = `rgb(${red} , ${green}, ${blue})`;
    return str;
};

const firstButton: any = document.querySelector(".first-button");
const secondButton: any = document.querySelector(".second-button");
const thirdButton: any = document.querySelector(".third-button");

const firstStream$ = fromEvent(firstButton, "click");
const secondStream$ = fromEvent(secondButton, "click");
const thirdStream$ = fromEvent(thirdButton, "click");

merge(firstStream$, secondStream$, thirdStream$).subscribe(
    (value) => (body.style.background = getRandBackColor())
);
