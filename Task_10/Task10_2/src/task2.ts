import { Observable, range } from "rxjs";
import { map, mergeMap, toArray } from "rxjs/operators";

const stream$: Observable<number> = range(1, 6);

stream$
    .pipe(
        toArray(),
        mergeMap((array) => array.reverse()),
        map((val) => {
            if (val === 1) {
                throw new Error("Error");
            } else {
                return val - 1;
            }
        })
    )
    .subscribe({
        next: console.log,
        error: console.log,
    });
