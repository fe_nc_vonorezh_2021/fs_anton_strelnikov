const path = require("path");

module.exports = {
    context: path.resolve(__dirname, "src"),
    entry: {
        task3: "./task3.js",
    },

    output: {
        path: path.resolve(__dirname, "dist"),
    },
};
