const path = require("path");

module.exports = {
    entry: "./Task_4/index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js",
    },
};
