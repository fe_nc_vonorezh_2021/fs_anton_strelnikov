// Генерируем массив
let arr = Array.from({length: 10}, () => Math.floor(Math.random() * 10));
// Функция с сортировками 
const sort = (arr, dir) => {
  switch(dir) {
    case 'asc':
      return arr.sort((a,b) => a - b)
	  case 'desc':
	    return arr.sort((a,b) => b - a)	
	}
}
// Сумма квадратов
let sum = arr.reduce((prev, curr) => curr % 2 ? prev + Math.pow(curr, 2) : prev , 0);
console.log("Сгенерированный массив:", arr)
console.log("По возрастанию:", sort(arr,'asc'));
console.log("По убыванию:", sort(arr,'desc'));
console.log("Сумма квадратов нечетных элементов:", sum);
