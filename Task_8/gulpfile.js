const gulp = require("gulp");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");

gulp.task("mini", function () {
    return gulp
        .src("./Task_4/*/*.js")
        .pipe(concat("index.js"))
        .pipe(uglify())
        .pipe(gulp.dest("./dist/"));
});
