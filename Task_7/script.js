const table = document.querySelector(".table");
const input = document.querySelector(".lowInput");
const edit = document.querySelector(".edit");
const del = document.querySelector(".delete");
const add = document.querySelector(".addRow");
const modal = document.getElementById("myModal");
const closebtn = document.querySelector(".close");

const addValue = () => {
    let target = document.querySelector(".active");
    target.innerHTML = input.value;
    input.value = "";
    check();
    modal.style.display = "none";
};

const deleteValue = () => {
    let target = document.querySelector(".active");
    target.innerHTML = "";
    input.value = "";
    check();
    modal.style.display = "none";
};

const check = () => {
    let active = document.querySelector(".active");
    if (active) {
        active.classList.toggle("active");
    }
};

const addRow = () => {
    const tableBody = table.getElementsByTagName("tbody")[0];
    const tableHead = table.getElementsByTagName("th");
    let button = "<td><button class='delRow' onclick ='deleteRow(this)'>-</button></td>";
    let newTd = "<td>example</td>".repeat(tableHead.length) + button;
    let newRow = tableBody.insertRow(tableBody.rows.length);
    newRow.innerHTML = newTd;
};

const deleteRow = (element) => {
    let row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
};

const handler = (event) => {
    let td = event.target.closest("td");
    let inp = event.target.closest("input");
    check();
    if (!td || !table.contains(td) || inp) {
        return;
    } else {
        td.classList.add("active");
        input.value = td.innerHTML;
        modal.style.display = "block";
    }
};

document.addEventListener("DOMContentLoaded", function () {
    edit.addEventListener("click", addValue);
    del.addEventListener("click", deleteValue);
    add.addEventListener("click", addRow);
    table.addEventListener("click", handler);
    closebtn.addEventListener("click", () => {
        modal.style.display = "none";
        check();
    });
});

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
        check();
    }
};
