import { ObjectFactory } from "./class/object-factory";
import { Market } from "./class/market";
import { Department } from "./class/department";
import { Product } from "./class/product";
import { Milk } from "./class/milk";
import { Bread } from "./class/bread";

//Tests
const factory = new ObjectFactory();
const Magnit = factory.create(Market, "Магнит", "Краснодар");
console.log(Magnit);
const bake = factory.create(Department, "Выпечка");
console.log(bake);
const dumplings = factory.create(Product, "Замороженные продукты", "Пельмени", new Date(2021, 11, 26), 10, 150);
console.log(dumplings);
dumplings.productEarn();
dumplings.productCheck();
const milk = factory.create(Milk, "Молочный отдел", "Простоквашино", new Date(2021, 11, 2), 10, 50);
console.log(milk);
milk.productCheck();
milk.productEarn();
