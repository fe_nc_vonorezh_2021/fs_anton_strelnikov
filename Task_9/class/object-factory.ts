import { ConstructorFunction } from "./types";

const log = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const value = descriptor.value;
    descriptor.value = function (...args: any[]) {
        console.log(`Object ${args[0].name} created!`);
        return value.apply(this, args);
    };
};

export class ObjectFactory {
    @log
    create<T>(object: ConstructorFunction<T>, ...args: any[]): T {
        return new object(...args);
    }
}
