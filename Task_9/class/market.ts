import { Department } from "./department";

interface IMarket {
    name: string;
    city: string;
    department: Department;
    income: number;
}

export class Market implements IMarket {
    public income: number;
    public department: Department;
    public constructor(public name: string, public city: string, public departmentName: string) {
        this.name = name;
        this.city = city;
        this.income = 0;
        this.department = new Department(departmentName);
    }
}
