import { Department } from "./department";

export class Product extends Department {
    public products?: string[];
    public constructor(public departmentName: string, public product: string, public date?: Date, public number?: number, public price?: number) {
        super(departmentName);
        this.product = product;
        this.products = ["Молоко", "Чипсы", "Хлеб", "Напитки"];
        this.date = date;
        this.number = number;
        this.price = price;
    }
    get productName(): string {
        return this.product;
    }

    set productName(value: string) {
        this.product = value;
    }

    get productsList(): string[] {
        return this.products;
    }

    set productsList(value: string[]) {
        this.products = value;
    }

    earn(): number {
        return this.number! * this.price!;
    }

    productCheck(): void {
        let nowDate: Date = new Date(2021, 11, 4);
        let diff: number = Math.ceil((this.date!.getTime() - nowDate.getTime()) / (1000 * 3600 * 24));
        if (diff <= 0) {
            console.log(`Срок годности товара(ов) ${this.product} истек на ${Math.abs(diff)} дня(дней)`);
        } else {
            console.log(`Срок годности товара(ов) ${this.product} еще не истек`);
        }
    }
    productEarn(): void {
        console.log(`Доход от продукта(ов) ${this.product!} ${this.earn()} рублей`);
    }
}
