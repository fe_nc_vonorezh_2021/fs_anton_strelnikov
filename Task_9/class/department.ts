export class Department {
    public constructor(public departmentName: string) {
        this.departmentName = departmentName;
    }

    get department(): string {
        return this.departmentName;
    }

    set department(value: string) {
        this.departmentName = value;
    }
}
