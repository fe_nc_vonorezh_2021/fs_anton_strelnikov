import { Product } from "./product";

export class Milk extends Product {
    private manufacturer: string;
    public constructor(public departmentName: string, public product: string, public date?: Date, public number?: number, public price?: number) {
        super(departmentName, product, date, number, price);
        this.manufacturer = "Voronezh";
    }
}
