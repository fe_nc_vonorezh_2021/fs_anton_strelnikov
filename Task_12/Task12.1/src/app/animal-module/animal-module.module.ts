import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AnimalComponent } from "./animal/animal.component";
import { AnimalDetailsComponent } from "./animal-details/animal-details.component";

@NgModule({
    declarations: [AnimalComponent, AnimalDetailsComponent],
    imports: [CommonModule],
    exports: [AnimalComponent, AnimalDetailsComponent],
})
export class AnimalModuleModule {}
