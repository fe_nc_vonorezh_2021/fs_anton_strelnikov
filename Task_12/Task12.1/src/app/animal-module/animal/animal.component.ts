import { Component, OnInit } from "@angular/core";
import { Animal } from "../animal";
import { AnimalService } from "../animal.service";

@Component({
    selector: "app-animal",
    templateUrl: "./animal.component.html",
    styleUrls: ["./animal.component.less"],
})
export class AnimalComponent implements OnInit {
    public selectedAnimal?: Animal;
    public hiddenCats: boolean = false;
    public animals: Animal[] = [];

    constructor(public animalService: AnimalService) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    onSelect(animal: Animal): void {
        this.selectedAnimal = animal;
    }

    getAnimals(): void {
        this.hiddenCats = false;
        this.animalService
            .getAnimals()
            .subscribe((animals) => (this.animals = animals));
    }
    getFilteredAnimals(): void {
        this.hiddenCats = true;
        this.animalService
            .getFilteredAnimals()
            .subscribe((animals) => (this.animals = animals));
    }

    logInfo(info: string) {
        console.log(info);
    }
}
