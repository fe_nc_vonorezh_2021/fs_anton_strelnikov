import { Component, OnInit, Input, Output } from "@angular/core";
import { Animal } from "../animal";
import { EventEmitter } from "@angular/core";
@Component({
    selector: "app-animal-details",
    templateUrl: "./animal-details.component.html",
    styleUrls: ["./animal-details.component.less"],
})
export class AnimalDetailsComponent {
    @Input() animal: Animal | undefined;
    @Output() animalInfoEvent = new EventEmitter<string>();
    constructor() {}

    sendInfo(value: string) {
        this.animalInfoEvent.emit(value);
    }
}
