import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { Animal } from "./animal";
import { Animals } from "./animal-list";

const enum Cats {
    animalType1 = "Кот",
    animalType2 = "Кошка",
}

@Injectable({
    providedIn: "root",
})
export class AnimalService {
    getAnimals(): Observable<Animal[]> {
        const animals = of(Animals);
        return animals;
    }

    getFilteredAnimals(): Observable<Animal[]> {
        const animals = of(Animals);
        const filteredAnimals = animals.pipe(
            map((animal) =>
                animal.filter(
                    (animal) =>
                        animal.type != Cats.animalType1 &&
                        animal.type != Cats.animalType2
                )
            )
        );
        return filteredAnimals;
    }
}
