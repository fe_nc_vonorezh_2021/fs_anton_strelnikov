import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AnimalComponent } from "./animal/animal.component";
import { AnimalDetailsComponent } from "./animal-details/animal-details.component";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { MatDialogModule } from "@angular/material/dialog";
import { ReactiveFormsModule } from "@angular/forms";
import { AddAnimalComponent } from "./add-animal/add-animal.component";

@NgModule({
    declarations: [AnimalComponent, AnimalDetailsComponent, AddAnimalComponent],
    imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule, MatDialogModule],
    exports: [AnimalComponent, AnimalDetailsComponent],
    entryComponents: [AddAnimalComponent],
})
export class AnimalModuleModule {}
