import { Component, Input } from "@angular/core";
import { Animal } from "../animal";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { AnimalService } from "../animal.service";
import { FormGroup, FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ViewChild } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";

@Component({
    selector: "app-add-animal",
    templateUrl: "./add-animal.component.html",
    styleUrls: ["./add-animal.component.less"],
})
export class AddAnimalComponent {
    @ViewChild("animalForm") form: any;
    private animals: Animal[] = [];
    public genders = ["Мальчик", "Девочка"];
    public animal = {
        id: "",
        name: "",
        type: "",
        color: "",
        age: "",
        gender: this.genders[0],
    };

    constructor(private route: ActivatedRoute, private animalService: AnimalService, private location: Location) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    onSubmit(): void {
        this.save();
    }

    save(): void {
        if (this.form.value) {
            this.animalService.addAnimal(this.form.value as Animal).subscribe((animal) => {
                this.animals.push(animal);
            });
        }
    }

    getAnimals(): void {
        this.animalService.getAnimals().subscribe((animals) => (this.animals = animals));
    }
}
