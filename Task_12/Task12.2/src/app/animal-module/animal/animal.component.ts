import { Component, ComponentFactoryResolver, OnInit } from "@angular/core";
import { Animal } from "../animal";
import { AnimalService } from "../animal.service";
import { MatDialog } from "@angular/material/dialog";
import { AddAnimalComponent } from "../add-animal/add-animal.component";
import { ViewEncapsulation } from "@angular/core";

import { ViewChild, ViewContainerRef, ComponentFactory, ComponentRef } from "@angular/core";

@Component({
    selector: "app-animal",
    templateUrl: "./animal.component.html",
    styleUrls: ["./animal.component.less"],
})
export class AnimalComponent implements OnInit {
    @ViewChild("formContainer", { read: ViewContainerRef }) container: any;
    componentRef!: ComponentRef<any>;
    public hiddenCats: boolean = false;
    public animals: Animal[] = [];
    public isModalShow: boolean = false;

    constructor(private resolver: ComponentFactoryResolver, public animalService: AnimalService) {}

    ngOnInit(): void {
        this.getAnimals();
    }

    getAnimals(): void {
        this.hiddenCats = false;
        this.animalService.getAnimals().subscribe((animals) => (this.animals = animals));
    }

    delete(animal: Animal): void {
        this.animals = this.animals.filter((h) => h !== animal);
        this.animalService.deleteAnimal(animal.id).subscribe();
    }

    closeDialog() {
        this.isModalShow = false;
        this.container.clear();
    }

    createComponent() {
        this.isModalShow = true;
        this.container.clear();
        const factory: ComponentFactory<AddAnimalComponent> = this.resolver.resolveComponentFactory(AddAnimalComponent);
        this.componentRef = this.container.createComponent(factory);
    }
}
