import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AnimalComponent } from "./animal-module/animal/animal.component";
import { AnimalDetailsComponent } from "./animal-module/animal-details/animal-details.component";
import { AddAnimalComponent } from "./animal-module/add-animal/add-animal.component";

const routes: Routes = [
    { path: "pets", component: AnimalComponent },
    { path: "", redirectTo: "/pets", pathMatch: "full" },
    { path: "pet/:id", component: AnimalDetailsComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
