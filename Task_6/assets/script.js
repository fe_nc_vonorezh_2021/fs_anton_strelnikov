const form = document.querySelector(".send-form");
const name = form.querySelector(".first-name");
const surname = form.querySelector(".last-name");
const email = form.querySelector(".email");
const phone = form.querySelector(".phone");
const message = form.querySelector(".message");
const fields = form.querySelectorAll(".field");
const btn = form.querySelector(".send-btn");

const validateEmail = (email, errFields) => {
    const re = /\S+@\S+\.\S+/;
    re.test(email.value) ? highlight(email, false) : (highlight(email, false), errFields.push(email.name));
};

const validatePhone = (phone, errFields) => {
    if (phone.value.length >= 4 && phone.value.length < 17) {
        highlight(phone, false);
        errFields.push(phone.name);
    } else if (phone.value.length === 17) {
        highlight(phone, true);
    }
};

const highlight = (target, error) => {
    !error ? target.classList.add("error") : target.classList.add("valid");
};

const removeHighlight = () => {
    fields.forEach((field) => {
        field.style.border = "none";
    });
    phone.style.border = "none";
    email.style.border = "none";
};

const checkFields = (errFields) => {
    fields.forEach((field) => {
        if (!field.value) {
            highlight(field, false);
            return errFields.push(field.name);
        } else {
            highlight(field, true);
        }
    });
    return errFields;
};

const createUser = (errFields) => {
    if (errFields.length === 0) {
        if (getCookie("flag") && getCookie("name") == name.value && getCookie("surname") == surname.value) {
            alert(`${getCookie("name")} ${getCookie("surname")}, ваше обращение обрабатывается!`);
        } else {
            alert(`${name.value} ${surname.value}, спасибо за обращение!`);
            let formData = new FormData(form);
            for (let [name, value] of formData) {
                console.log(`${name} = ${value}`);
            }
            setCookie("name", name.value);
            setCookie("surname", surname.value);
            setCookie("flag", true);
        }
        removeHighlight();
        localStorage.clear();
        form.reset();
    } else {
        alert(`Поля ${errFields.join(", ")} заполнены не верно, пожалуйста исправьте!`);
        console.log(errFields);
    }
};

const setCookie = (name, value, options = {}) => {
    options = {
        path: "/",
        ...options,
    };
    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }
    document.cookie = updatedCookie;
};

const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : null;
};

const activeButton = (event) => {
    const required = form.querySelectorAll(".required");
    const target = event.target;
    localStorage.setItem(target.id.toString(), target.value);
    !Array.from(required).some((item) => !item.value) ? btn.classList.remove("notActive") : btn.classList.add("notActive");
};

const validation = (event) => {
    event.preventDefault();
    let errFields = [];
    checkFields(errFields);
    validateEmail(email, errFields);
    validatePhone(phone, errFields);
    createUser(errFields, form);
};

const getUserInfo = () => {
    const arr = [name, surname, email, phone, message];
    arr.map((element) => {
        element.value = localStorage.getItem(`${element.id}`);
    });
};

const setCursorPosition = (pos, elem) => {
    elem.focus();
    if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    else if (elem.createTextRange) {
        const range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select();
    }
};

const mask = (event) => {
    const target = event.target;
    let matrix = "+7(___)-__-__-___",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = target.value.replace(/\D/g, "");
    if (def.length >= val.length) val = def;
    target.value = matrix.replace(/./g, function (a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a;
    });
    if (event.type == "blur") {
        if (target.value.length == 2) target.value = "";
    } else setCursorPosition(target.value.length, target);
};

const collected = () => {
    getUserInfo();
    form.addEventListener("submit", validation);
    form.addEventListener("input", activeButton);
    phone.addEventListener("input", mask, false);
    phone.addEventListener("focus", mask, false);
    phone.addEventListener("blur", mask, false);
};

window.addEventListener("DOMContentLoaded", collected);
