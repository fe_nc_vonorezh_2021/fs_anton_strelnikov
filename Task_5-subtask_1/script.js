const buttons = document.getElementsByTagName("button");
const input = document.getElementsByTagName("input")[0];
let ul = document.getElementById("list");
let expression = [];
let operator = false;

const getNumber = (i) => {
    return () => {
        if (buttons[i].innerHTML === "÷") {
            addOperator("/");
        } else if (buttons[i].innerHTML === "x") {
            addOperator("*");
        } else if (buttons[i].innerHTML === "+") {
            addOperator("+");
        } else if (buttons[i].innerHTML === "-") {
            addOperator("-");
        } else if (buttons[i].innerHTML === "xⁿ") {
            addOperator("**");
        } else {
            if (typeof expression[expression.length - 1] == "number") {
                expression = [];
                expression.push(buttons[i].innerHTML);
                operator = true;
            } else {
                expression.push(buttons[i].innerHTML);
            }
            if (operator) {
                input.value = buttons[i].innerHTML;
            } else {
                input.value += buttons[i].innerHTML;
            }
            operator = false;
        }
    };
};

const calc = () => {
    return function () {
        if (expression.length === 0) {
            return;
        } else {
            let hist = expression.join("");
            let result = eval(hist);
            history(hist, result);
            input.value = result;
            expression = [];
            expression.push(result);
            operator = false;
        }
    };
};

const addOperator = (action) => {
    if (operator) {
        expression.pop();
        expression.push(action);
    } else {
        expression.push(action);
        operator = true;
    }
};

function clear() {
    return () => {
        input.value = "";
        expression = [];
        operator = false;
        ul.innerHTML = "";
    };
}

const sqrt = function () {
    return () => {
        var number = input.value;
        var sqrt = Math.sqrt(number);
        expression.pop();
        expression.push(sqrt);
        input.value = sqrt;
        const li = document.createElement("li");
        li.appendChild(document.createTextNode("√" + number + " = " + sqrt));
        ul.appendChild(li);
    };
};

function history(hist, answer) {
    hist = hist + " = " + answer;
    console.log(hist);
    const li = document.createElement("li");
    li.appendChild(document.createTextNode(hist));
    ul.appendChild(li);
}

for (var i = 0; i < buttons.length; i++) {
    if (buttons[i].innerHTML === "=") {
        buttons[i].addEventListener("click", calc(i));
    } else if (buttons[i].innerHTML === "√") {
        buttons[i].addEventListener("click", sqrt(i));
    } else if (buttons[i].innerHTML === "C") {
        buttons[i].addEventListener("click", clear(i));
    } else {
        buttons[i].addEventListener("click", getNumber(i));
    }
}
