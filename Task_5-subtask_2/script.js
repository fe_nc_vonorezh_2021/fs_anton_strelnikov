class Market {
    constructor(name, city, departmentName) {
        this.name = name;
        this.city = city;
        this.income = 0;
        this.department = new Department(departmentName);
    }
}

class Department {
    constructor(departmentName) {
        this.departmentName = departmentName;
    }

    get department() {
        return this._departmentName;
    }

    set department(value) {
        this._departmentName = value;
    }

    earn() {
        return this.number * this.price;
    }
}

class Product extends Department {
    constructor(product, date, number) {
        super(name);
        this.product = product;
        this.products = ["Молоко", "Чипсы", "Хлеб", "Напитки"];
        this.date = new Date(date);
        this.number = number;
    }

    get product() {
        return this._product;
    }

    set product(value) {
        this._product = value;
    }

    get products() {
        return this._products;
    }

    set products(value) {
        this._products = value;
    }

    productCheck() {
        let nowDate = new Date(2021, 11, 4);
        let diff = Math.ceil((this.date.getTime() - nowDate.getTime()) / (1000 * 3600 * 24));
        if (diff <= 0) {
            console.log(`Срок годности товара(ов) ${this.product} истек на ${Math.abs(diff)} дня(дней)`);
        } else {
            console.log(`Срок годности товара(ов) ${this.product} еще не истек`);
        }
    }

    earn() {
        console.log(`Доход от продукта(ов) ${this.product} ${super.earn()} рублей`);
    }
}

class Milk extends Product {
    constructor(product, price, date) {
        super(product, date);
        this.price = price;
        this.manufacturer = "Voronezh";
        this.number = 50;
    }
}

class Bread extends Product {
    constructor(product, price, date) {
        super(product, date);
        this.price = price;
        this.manufacturer = "Lipetsk";
        this.number = 30;
    }
}
//Test
let Magnit = new Market("Магнит", "Краснодар", "ХозТовары");
console.log(Magnit.department);

let bake = new Department("Выпечка");
console.log(bake.departmentName);

let pelm = new Product("Пельмени", new Date(2021, 11, 2), 14);
console.log(pelm.product);
pelm.product = "Новые пельмени";
console.log(pelm.product);
console.log(pelm.products);
pelm.productCheck();

let milk2 = new Milk("Простоквашино", 30, new Date(2021, 11, 10));
milk2.productCheck();
milk2.earn();

let bread2 = new Bread("Хлебозавод", 20, new Date(2021, 11, 10));
bread2.earn();
bread2.productCheck();
// Change price
console.log(bread2.price);
delete bread2.price;
console.log(bread2.price);
bread2.price = 40;
bread2.earn();
